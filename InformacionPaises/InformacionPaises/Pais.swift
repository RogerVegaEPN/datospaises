//
//  Pais.swift
//  InformacionPaises
//
//  Created by Roger on 4/12/17.
//  Copyright © 2017 Roger. All rights reserved.
//

import Foundation
import ObjectMapper

class Pais : Mappable{
    var nombrePais:String?
    var capitalPais:String?
    var continentePais:String?
    var poblacionPais:Int?
    var banderaPais:String?
    
    required init?(map:Map){}
    
    func mapping(map: Map) {
        nombrePais <- map["name"]
        capitalPais <- map["capital"]
        continentePais <- map["region"]
        poblacionPais <- map["population"]
        banderaPais <- map["flag"]
        
    }
    
}
