//
//  ConsultarViewController.swift
//  InformacionPaises
//
//  Created by Roger on 4/12/17.
//  Copyright © 2017 Roger. All rights reserved.
//

import UIKit

class ConsultarViewController: UIViewController {
    
    
    @IBOutlet weak var paisTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func consultarButton(_ sender: Any) {
        if paisTextField.text != ""
        {
            performSegue(withIdentifier: "segue", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let resultadoViewController = segue.destination as! ResultadoViewController
        resultadoViewController.pais =  paisTextField.text!
    }
    
    @IBOutlet weak var regresarButton: UIButton!
    @IBAction func regresarButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
}
