//
//  ResultadoViewController.swift
//  InformacionPaises
//
//  Created by Roger on 4/12/17.
//  Copyright © 2017 Roger. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class ResultadoViewController: UIViewController {

    var pais = String()
    var paisArray = [Pais]()
    var numeroResultado = 0
    
    @IBOutlet weak var totalResultadosLbl: UILabel!
    @IBOutlet weak var totalResultados2Lbl: UILabel!
    @IBOutlet weak var resultadoMostradoLbl: UILabel!
    @IBOutlet weak var paisLbl: UILabel!
    @IBOutlet weak var capitalLbl: UILabel!
    @IBOutlet weak var continenteLbl: UILabel!
    @IBOutlet weak var poblacionLbl: UILabel!
    
    @IBOutlet weak var siguienteButton: UIButton!
    @IBOutlet weak var anteriorButton: UIButton!
    
    @IBOutlet weak var banderaWebView: UIWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        Alamofire.request("https://restcountries.eu/rest/v2/name/\(pais)").responseArray { (response: DataResponse<[Pais]>) in

            if(response.value == nil){
                self.alertaSinResultados()
            } else{
                self.paisArray = response.result.value!
                
                DispatchQueue.main.async {
                    
                    self.totalResultadosLbl.text = String(self.paisArray.count)
                    self.totalResultados2Lbl.text = String(self.paisArray.count)
                    self.resultadoMostradoLbl.text = String(self.numeroResultado)
                    
                    self.anteriorButton.isEnabled = false
                    if self.paisArray.count == 1 {
                        self.siguienteButton.isEnabled = false
                    }
                    
                    self.mostrarDatos(numeroResultado: self.numeroResultado)
                }
            }


        }
    }

    @IBAction func siguienteButton(_ sender: Any) {
        if(self.numeroResultado < self.paisArray.count - 1)
        {
            anteriorButton.isEnabled = true
            self.numeroResultado += 1
            if(self.numeroResultado == self.paisArray.count - 1){
                siguienteButton.isEnabled = false
            }
            self.mostrarDatos(numeroResultado: self.numeroResultado)
        }
    }
    
    @IBAction func anteriorButton(_ sender: Any) {
        if(self.numeroResultado > 0)
        {
            siguienteButton.isEnabled = true
            anteriorButton.isEnabled = true
            self.numeroResultado -= 1
            if(self.numeroResultado == 0){
                anteriorButton.isEnabled = false
            }
            self.mostrarDatos(numeroResultado: self.numeroResultado)
        }
    }
    
    
    func mostrarDatos(numeroResultado: Int){
        self.paisLbl.text = self.paisArray[numeroResultado].nombrePais
        self.resultadoMostradoLbl.text = String(self.numeroResultado + 1)
        let url = NSURL(string: self.paisArray[numeroResultado].banderaPais!)
        let request  = NSURLRequest(url: url! as URL)
        self.banderaWebView.frame.size.height = 1
        self.banderaWebView.frame.size = self.banderaWebView.sizeThatFits(.zero)
        self.banderaWebView.scalesPageToFit = true
        self.banderaWebView.scrollView.isScrollEnabled=false;
        self.banderaWebView.loadRequest(request as URLRequest)

        self.capitalLbl.text = self.paisArray[numeroResultado].capitalPais
        self.poblacionLbl.text = String(describing: self.paisArray[numeroResultado].poblacionPais!)
        self.continenteLbl.text = self.paisArray[numeroResultado].continentePais
    }
    
    @IBAction func RegresarButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func alertaSinResultados(){
        print("No se encontraron resultados");
        let alertController = UIAlertController(title: "Informacion", message: "No se han encontrado resultados!!!", preferredStyle: .alert)
        let acceptAction = UIAlertAction(title: "Aceptar", style: .default){
             (action) in
            self.dismiss(animated: true, completion: nil)
            }
        alertController.addAction(acceptAction)
        present(alertController, animated: true, completion: nil)
    }

}
